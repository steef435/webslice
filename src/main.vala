class ws.Main : Object {
	/**
	 * The base path to use
	 */
	static string? base_path = Path.build_filename(Environment.get_home_dir(), ".ws");

	/**
	 * The name of the slice session to run
	 */
	static string? session = null;

	/**
	 * The name of the slice to run
	 */
	[CCode (array_null_terminated = true)]
	static string[]? slice = null;

	/**
	 * Command line options
	 */
	const OptionEntry[] options = {
		{ "base-directory", 'b', 0, OptionArg.FILENAME, ref base_path, "Directory containing webslices (default: ~/.wd)", "BASE_DIRECTORY" },
		{ "session", 's', 0, OptionArg.STRING, ref session, "Slice session to run (default: default)", "SESSION" },
		{ "", 0, 0, OptionArg.STRING_ARRAY, ref slice, "Slice to run", "SLICE" },
		{ null }
	};

	/**
	 * Display error and reference to program_name --help
	 *
	 * @param error Error to display
	 * @param program_name Program name to use (usually argv[0])
	 */
	static void cmd_err(string error, string program_name) {
		stderr.printf("error: %s\nsee %s --help\n", error, program_name);
	}

	/**
	 * Main function
	 */
	static int main(string[] argv) {
		Webslice webslice = null;
		Window window;

		/* Parse command line */
		try {
			var oc = new OptionContext(null);
			oc.set_help_enabled(true);
			oc.add_main_entries(options, null);
			oc.add_group(Gtk.get_option_group(true));
			oc.parse(ref argv);
		} catch (OptionError e) {
			cmd_err(e.message, argv[0]);
			return 1;
		}

		/* Handle command line */
		if (slice == null) {
			cmd_err("no slice specified", argv[0]);
			return 1;
		}

		if (slice.length > 1) {
			cmd_err("too many arguments (one slice name required)", argv[0]);
			return 1;
		}

		if (base_path == null)
			base_path = Path.build_filename(Environment.get_home_dir(), ".ws");

		if (session == null)
			session = "default";

		/* Load webslice */
		try {
			webslice = ws.Webslice.webslice(slice[0], session, base_path);
		} catch (FileError e) {
			cmd_err(e.message, argv[0]);
		} catch (Json.ParserError e) {
			cmd_err(e.message.replace("<data>", Path.build_filename(base_path, slice[0], "slice.json")), argv[0]);
		} catch (Error e) {
			cmd_err(e.message.replace("<data>", Path.build_filename(base_path, slice[0], "slice.json")), argv[0]);
		}
		if (webslice == null)
			return 1;

		/* Create session directory */
		try {
			File.new_for_path(webslice.session_path).make_directory();
		} catch (Error e) {
			if (!(e is IOError.EXISTS)) {
				stderr.printf("error creating session: %s\n", e.message);
				return 1;
			}
		}

		/* Load script */
		try {
			webslice.load_script();
		} catch (FileError e) {
			stderr.printf("error loading script.js: %s\n", e.message);
			return 1;
		}

		/* Load style */
		try {
			webslice.load_style();
		} catch (FileError e) {
			stderr.printf("error loading style.css: %s\n", e.message);
			return 1;
		}

		/* Load web session */
		if (webslice.keep_web_session) {
			try {
				webslice.load_web_session();
			} catch (FileError e) {
				stderr.printf("error loading web session: %s\n", e.message);
				return 1;
			}
		}

		/* Create window */
		window = new Window(webslice);
		window.destroy.connect(Gtk.main_quit);
		window.show_all();

		Gtk.main();

		/* Save web session */
		if (webslice.keep_web_session) {
			try {
				webslice.save_web_session();
			} catch (FileError e) {
				stderr.printf("error saving web session: %s\n", e.message);
				return 1;
			}
		}

		return 0;
	}
}
