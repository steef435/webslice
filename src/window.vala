class ws.Window : Gtk.Window {
	WebKit.WebView view;

	public Window(Webslice slice) {
		string session_path;
		WebKit.WebContext context;
		WebKit.WebsiteDataManager? data_manager;

		session_path = Path.build_filename(slice.path, slice.session);

		/* Web data */
		data_manager = null;
		if (slice.keep_web_data)
			data_manager = Object.new(typeof(WebKit.WebsiteDataManager), base_data_directory: Path.build_filename(slice.session_path, "web_data")) as WebKit.WebsiteDataManager;

		/* Context */
		if (data_manager != null)
			context = new WebKit.WebContext.with_website_data_manager(data_manager);
		else
			context = new WebKit.WebContext();

		/* Cookies */
		if (slice.keep_cookies)
			context.get_cookie_manager().set_persistent_storage(Path.build_filename(slice.session_path, "cookies"), WebKit.CookiePersistentStorage.TEXT);

		/* Create view */
		view = Object.new(typeof(WebKit.WebView), settings: slice, web_context: context) as WebKit.WebView;

		/* User content */
		if (slice.script != null)
			view.user_content_manager.add_script(slice.script);

		if (slice.style != null)
			view.user_content_manager.add_style_sheet(slice.style);

		/* Web session */
		if (slice.web_session != null) {
			/* Restore session */
			view.restore_session_state(slice.web_session);
			view.go_to_back_forward_list_item(view.get_back_forward_list().get_current_item()); /* Required reload */
		} else {
			/* Load URI */
			view.load_uri(slice.uri);
		}

		if (slice.keep_web_session) {
			/* Save session on close */
			delete_event.connect(() => {
				slice.web_session = view.get_session_state();
				return false;
			});
		}

		/* Title */
		view.bind_property("title", this, "title");

		add(view);
	}
}
