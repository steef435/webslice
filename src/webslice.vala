/**
 * Stores WebSlice configuration
 */
class ws.Webslice : WebKit.Settings {
	/**
	 * Name
	 */
	public string name;

	/**
	 * Session
	 */
	public string session;

	/**
	 * Path to webslice (without session)
	 */
	public string path;

	/**
	 * Path to webslice session
	 */
	public string session_path;

	/**
	 * script.js
	 */
	public WebKit.UserScript? script = null;

	/**
	 * style.css
	 */
	public WebKit.UserStyleSheet? style = null;

	/**
	 * web_session
	 */
	public WebKit.WebViewSessionState? web_session = null;

	/**
	 * URI
	 *
	 * The URI to be loaded when the webslice starts.
	 */
	public string uri { get; set; default = "about:blank"; }

	/**
	 * Whether to save persistent cookies
	 */
	public bool keep_cookies { get; set; default = true; }

	/**
	 * Whether to save website data
	 */
	public bool keep_web_data { get; set; default = true; }

	/**
	 * Whether to save the web session (current URI and history)
	 *
	 * If this is true, URI will be ignored if a session exists.
	 */
	public bool keep_web_session { get; set; default = false; }

	/**
	 * Load script.js
	 */
	public WebKit.UserScript? load_script() throws FileError {
		script = null;

		try {
			string data;
			FileUtils.get_contents(Path.build_filename(path, "script.js"), out data);
			script = new WebKit.UserScript(data, WebKit.UserContentInjectedFrames.ALL_FRAMES, WebKit.UserScriptInjectionTime.END, null, null);
		} catch (FileError e) {
			if (!(e is FileError.NOENT)) {
				throw e;
			}
		}

		return script;
	}

	/**
	 * Load style.css
	 */
	public WebKit.UserStyleSheet? load_style() throws FileError {
		style = null;

		try {
			string data;
			FileUtils.get_contents(Path.build_filename(path, "style.css"), out data);
			style = new WebKit.UserStyleSheet(data, WebKit.UserContentInjectedFrames.ALL_FRAMES, WebKit.UserStyleLevel.USER, null, null);
		} catch (FileError e) {
			if (!(e is FileError.NOENT)) {
				throw e;
			}
		}

		return style;
	}

	/**
	 * Load web_session
	 */
	public WebKit.WebViewSessionState? load_web_session() throws FileError {
		string filename;

		assert(keep_web_session);

		web_session = null;
		filename = Path.build_filename(session_path, "web_session");

		try {
			uint8[] data;
			FileUtils.get_data(filename, out data);

			web_session = new WebKit.WebViewSessionState(new Bytes(data));
			if (web_session == null)
				throw new FileError.FAILED(filename + ": File does not contain a valid WebViewSessionState");
		} catch (FileError e) {
			if (!(e is FileError.NOENT)) {
				throw e;
			}
		}

		return web_session;
	}

	/**
	 * Save web_session
	 */
	public void save_web_session() throws FileError {
		uint8[] data;

		data = web_session.serialize().get_data();
		FileUtils.set_data(Path.build_filename(session_path, "web_session"), data);
	}

	/**
	 * Load a webslice
	 *
	 * @param name Name of the webslice to load
	 * @param session Name of the session to use
	 * @param base_path Directory where the webslice to load is a subdirectory of
	 * @return A webslice for name in base_path
	 */
	public static Webslice webslice(string name, string session, string base_path) throws Error {
		string data;
		Webslice slice;

		/* Load data */
		FileUtils.get_contents(Path.build_filename(base_path, name, "slice.json"), out data, null);

		/* Parse properties */
		slice = Json.gobject_from_data(typeof(Webslice), data) as Webslice;
		assert (slice != null);

		/* Additional properties */
		slice.name = name;
		slice.session = session;
		slice.path = Path.build_filename(base_path, name);
		slice.session_path = Path.build_filename(slice.path, session);

		return slice;
	}
}
