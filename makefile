PREFIX ?= /usr

SRC := src/*.vala
PKG := json-glib-1.0 webkit2gtk-4.0

ws: $(SRC)
	valac -o ws $(SRC) $(foreach p, $(PKG), --pkg $p)

install: ws
	install -Dm755 ws $(DESTDIR)$(PREFIX)/bin/ws
